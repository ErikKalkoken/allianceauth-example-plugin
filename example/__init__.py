"""Example plugin app for Alliance Auth."""

# pylint: disable = invalid-name
default_app_config = "example.apps.ExampleConfig"

__version__ = "0.1.0"
